<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

use App\Repositories\PostRepository;
use App\Helpers\Contracts\ImageContract;

class PostController extends Controller
{
    protected $post;

    public function __construct(PostRepository $postRepository)
    {
        $this->post = $postRepository;
    }

    public function index()
    {
        $values = $this->post->index(10);

        return view('admin.posts.index', $values);
    }

    public function create()
    {
        $result = $this->post->createPost();

        return view('admin.posts.create', $result);
    }

    public function store(PostRequest $postRequest, ImageContract $image)
    {
        $result = $this->post->storePost($postRequest, $image);

        if ($result) {
            $redirect = redirect()->route('admin.posts.index')->with('success', 'Пост успешно создан!');
        } else {
            $redirect = redirect()->route('admin.posts.index')->with('error', 'Не удалось создать пост!');
        }

        return $redirect;
    }

    public function edit($id)
    {
        $result = $this->post->edit($id);

        return view('admin.posts.edit', $result);
    }

    public function update(PostRequest $postRequest, ImageContract $image, $id)
    {
        $result = $this->post->updatePost($postRequest, $image, $id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Пост успешно изменен!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось изменить пост!');
        }

        return $redirect;
    }

    public function destroy($id)
    {
        $result = $this->post->delete($id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Пост удален!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось удалить пост!');
        }

        return $redirect;
    }

    public function sort(Request $request)
    {
        $result = $this->post->sort($request, 10, ['category', 'tags']);

        return view('admin.posts.index', $result);
    }

    public function search(Request $request)
    {
        $result = $this->post->search($request, 10);

        return view('admin.posts.index', $result);
    }

}
