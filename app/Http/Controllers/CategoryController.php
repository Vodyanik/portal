<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->category = $categoryRepository;
    }

    public function index ()
    {
        $result = $this->category->index(10);

        return view('admin.categories.index', $result);
    }

    public function create()
    {
        $categories = $this->category->createCategory();

        return view('admin.categories.create', $categories);
    }

    public function store(CategoryRequest $categoryRequest)
    {
        $category_id = $this->category->storeCategory($categoryRequest);

        return redirect()->route('admin.categories.edit', ['id' => $category_id]);
    }

    public function edit($id)
    {
        $result = $this->category->edit($id);

        return view('admin.categories.edit', $result);
    }

    public function update(CategoryRequest $categoryRequest, $id)
    {
        $result = $this->category->updateCategory($categoryRequest, $id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Данные успешно изменены!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось внести изменения!');
        }

        return $redirect;
    }

    public function destroy($id)
    {
        if ($this->category->delete($id)) {
            $redirect = redirect()->back()->with('success', 'Категория удалена!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось удалить категорию!');
        }

        return $redirect;
    }

    public function sort(Request $request)
    {
        $result = $this->category->sort($request, 10, 'category');

        return view('admin.categories.index', $result);
    }

    public function search(Request $request)
    {
        $result = $this->category->search($request);

        return view('admin.categories.index', $result);
    }
    
}
