<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Contracts\ImageContract;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserRepository $userRepository)
    {
        $this->user = $userRepository;
    }

    public function index()
    {
        $users = $this->user->index(10);

        return view('admin.users.index', $users);
    }

    public function edit($id)
    {
        $values = $this->user->edit($id);

        return view('admin.users.edit', $values);
    }

    public function update(Request $request, ImageContract $image, $id)
    {
        $result = $this->user->updateUser($request, $image, $id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Данные успешно изменены!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось внести изменения!');
        }

        return $redirect;
    }

    public function destroy($id)
    {
        if ($this->user->delete($id)) {
            $redirect = redirect()->back()->with('success', 'Пользователь удален!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось удалить пользователя!');
        }

        return $redirect;
    }

    public function sort(Request $request)
    {
        $result = $this->user->sort($request,10, ['roles']);

        return view('admin.users.index', $result);
    }

    public function search(Request $request)
    {
        $result = $this->user->search($request, 10);

        return view('admin.users.index', $result);
    }

}

