<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TagRepository;

class TagController extends Controller
{
    protected $tag;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tag = $tagRepository;
    }

    public function index(Request $request)
    {
        $values = $this->tag->index(10);

        return view('admin.tags.index', $values);
    }

    public function create()
    {
        return view('admin.tags.create');
    }

    public function store(Request $request)
    {
        $result = $this->tag->store($request);

        if (!empty($result)) {
            $redirect = redirect()->route('admin.tags.index')->with('success', 'Тег успешно создан!');
        } else {
            $redirect = redirect()->route('admin.tags.index')->with('error', 'Не удалось создать тег!');
        }

        return $redirect;
    }

    public function edit($id)
    {
        $tag = $this->tag->edit($id);

        return view('admin.tags.edit', $tag);
    }

    public function update(Request $request, $id)
    {
        $result = $this->tag->update($request->all(), $id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Тег изменен!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось изменить тег!');
        }

        return $redirect;
    }

    public function destroy($id)
    {
        $result = $this->tag->delete($id);

        if ($result) {
            $redirect = redirect()->back()->with('success', 'Тег удален!');
        } else {
            $redirect = redirect()->back()->with('error', 'Не удалось удалить тег!');
        }

        return $redirect;
    }

    public function sort(Request $request)
    {
        $result = $this->tag->sort($request, 10);

        return view('admin.tags.index', $result);
    }

    public function search(Request $request)
    {
        $result = $this->tag->search($request, 10);

        return view('admin.tags.index', $result);
    }

}
