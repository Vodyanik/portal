<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $user = User::findOrFail(30);
//        $user->delete();

//        $role = Role::findOrFail(1);

//        dump($user->roles()->whereKey(1)->first());
//        dump($role->users()->whereKey(2)->first());

        return view('front.home');
    }
}
