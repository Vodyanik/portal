<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagRepository extends AdminBaseRepository
{
    public function model()
    {
        return Tag::class;
    }

    public function index($perPage)
    {
        $result = $this->paginate($perPage);

        return compact('result');
    }

    public function store(Request $request)
    {
        $result = $this->create($request->all());

        return $result;
    }

    public function edit($id)
    {
        $tag = $this->find($id);

        return compact('tag');
    }

}
