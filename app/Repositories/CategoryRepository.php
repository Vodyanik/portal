<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;

class CategoryRepository extends AdminBaseRepository
{

    public function model()
    {
        return Category::class;
    }

    public function index($perPage)
    {
        $result = $this->model->with('category')->paginate($perPage);

        return compact('result');
    }

    public function createCategory()
    {
        $categories = $this->all();

        return compact('categories');
    }

    public function storeCategory(CategoryRequest $categoryRequest)
    {
        $category = $this->create($categoryRequest->all());

        return $category->id;
    }

    public function edit($id)
    {
        $category = $this->findOrFail($id);
        $categories = $this->all();

        return compact('category', 'categories');
    }

    public function updateCategory(CategoryRequest $categoryRequest, $id)
    {
        $result = $this->update($categoryRequest->all(), $id);

        return $result;
    }

}
