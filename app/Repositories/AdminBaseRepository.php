<?php

namespace App\Repositories;

use Czim\Repository\BaseRepository;
use Illuminate\Http\Request;

abstract class AdminBaseRepository extends BaseRepository
{

    public function sort(Request $request, $perPage = 10, $relations = [])
    {
        if (empty($request->type)) {
            $type = 'desc';
        } else {
            $type = $request->type;
        }

        if (isset($request->name) && isset($type)) {
            if($request->type == 'asc') {
                $type = 'desc';
            } elseif($request->type == 'desc') {
                $type = 'asc';
            }

            $result = $this->model->orderBy($request->name, $type)->paginate($perPage);

            if (!empty($relations)) {
                $result->load($relations);
            }
        }

        $result->appends($request->query());

        return compact('result', 'type');
    }

    public function search(Request $request, $perPage = 10)
    {
        $result = $this->model->search($request->input('query'))->paginate($perPage);
        $query = $request->input('query');

        return compact('result', 'query');
    }

}
