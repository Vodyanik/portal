<?php

namespace App\Repositories;

use App\Helpers\Contracts\ImageContract;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Role;

class UserRepository extends AdminBaseRepository
{
    protected $user;

    private $path = 'images/users/';

    public function model()
    {
        return User::class;
    }

    public function index($perPage)
    {
        $result = $this->model->with('roles')->paginate($perPage);

        return compact('result');
    }

    public function edit($id)
    {
        $user = $this->findOrFail($id);
        $roles = Role::all();

        return compact('user', 'roles');
    }

    public function updateUser(Request $request, ImageContract $image, $id)
    {
        $roles = Role::all();
        $user = $this->findOrFail($id);

        $user->fill($request->all(), $id);

        if (!empty($request->image)) {
            $this->path = $this->path . '/' . $id;
            $name = $image->save($request->image, $this->path);
            $user->image = $this->path . '/' . $name;
        }
        
        $arrayRoles = [];
        foreach ($roles as $role) {
            if ($request->has($role->name) && $request->input($role->name) == 'on') {
                $arrayRoles[] = $role->id;
            }
        }
        
        $user->roles()->sync($arrayRoles);

        $saveResult = $user->save();

        return $saveResult;
    }
    
}