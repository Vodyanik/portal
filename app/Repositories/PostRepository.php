<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Helpers\Contracts\ImageContract;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;

class PostRepository extends AdminBaseRepository
{
    private $path = 'images/posts/';

    public function model()
    {
        return Post::class;
    }

    public function index($perPage)
    {
        $result = $this->model->with(['category', 'tags'])->paginate($perPage);

        return compact('result');
    }

    public function createPost()
    {
        $categories = Category::all();
        $tags = Tag::all();

        return compact('categories', 'tags');
    }

    public function storePost(PostRequest $postRequest, ImageContract $image)
    {
        $post = $this->make($postRequest->all());

        if (!empty($postRequest->image)) {
            $name = $image->save($postRequest->image, $this->path);
            $post->image = $this->path . $name;
        }

        $result = $post->save();

        if ($result) {
            $post->tags()->attach($postRequest->tag_ids);
        }

        return $result;
    }

    public function edit($id)
    {
        $post = $this->find($id);
        $categories = Category::all();
        $tags = Tag::all();

        return compact('post', 'categories', 'tags');
    }

    public function updatePost(PostRequest $postRequest, ImageContract $image, $id)
    {
        $post = $this->find($id);

        $post->update($postRequest->all());

        if (!empty($postRequest->image)) {
            $name = $image->save($postRequest->image, $this->path);
            $post->image = $this->path . $name;
        }

        $post->tags()->sync($postRequest->tag_ids);

        $result = $post->save();

        return $result;
    }

}
