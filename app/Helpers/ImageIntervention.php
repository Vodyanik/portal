<?php

namespace App\Helpers;

use App\Helpers\Contracts\ImageContract;
use Illuminate\Http\Request;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class ImageIntervention implements ImageContract {

    public function save($image, $path)
    {
        if (!File::exists(public_path($path))) {
            File::makeDirectory(public_path($path), 777, true);
        }
        $name = md5($image->getClientOriginalName()) . str_random(8) . '.jpg';
        Image::make($image)->save(public_path($path . '/' . $name));

        return $name;
    }

}