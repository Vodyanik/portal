<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tag extends Model
{
    use Searchable;

    protected $fillable = [
        'name'
    ];

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post');
    }
}
