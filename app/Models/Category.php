<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Category extends Model
{
    use Searchable;

    protected $fillable = [
        'name',
        'parent_category_id'
    ];

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_category_id');
    }
}
