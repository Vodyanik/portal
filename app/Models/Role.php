<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;

class Role extends Model
{
    use Searchable;

    protected $fillable = [
        'id',
        'name'
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
