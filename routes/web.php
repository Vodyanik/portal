<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);

    /*
     * Users
     */
    Route::get('users/sort', ['as' => 'admin.users.sort', 'uses' => 'UserController@sort']);
    Route::get('users/search', ['as' => 'admin.users.search', 'uses' => 'UserController@search']);
    Route::resource('users', 'UserController', [
        'as' => 'admin',
        'expect' => ['show'],
    ]);

    /*
     * Categories
     */
    Route::get('categories/sort', ['as' => 'admin.categories.sort', 'uses' => 'CategoryController@sort']);
    Route::get('categories/search', ['as' => 'admin.categories.search', 'uses' => 'CategoryController@search']);
    Route::resource('categories', 'CategoryController', [
        'as' => 'admin',
        'expect' => ['show'],
    ]);

    /*
     * Posts
     */
    Route::get('posts/sort', ['as' => 'admin.posts.sort', 'uses' => 'PostController@sort']);
    Route::get('posts/search', ['as' => 'admin.posts.search', 'uses' => 'PostController@search']);
    Route::resource('posts', 'PostController', [
        'as' => 'admin',
        'expect' => ['show'],
    ]);

    /*
     * Tags
     */
    Route::get('tags/sort', ['as' => 'admin.tags.sort', 'uses' => 'TagController@sort']);
    Route::get('tags/search', ['as' => 'admin.tags.search', 'uses' => 'TagController@search']);
    Route::resource('tags', 'TagController', [
        'as' => 'admin',
        'expect' => ['show'],
    ]);


});
