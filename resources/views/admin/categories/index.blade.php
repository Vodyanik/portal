@extends('admin.layouts.app')

@push('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/core.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/effects.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/extensions/cookie.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_all.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/trees/fancytree_childcounter.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/extra_trees.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
@endpush

@section('content')

    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">

                <div class="col-md-3">
                    <h5 class="panel-title"><a href="{{ route('admin.categories.index') }}">Категории</a></h5>
                </div>

                <div class="col-md-6">
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-success">Новая категория</a>
                </div>

                <div class="col-md-3">
                    <form action="{{ route('admin.categories.search') }}" method="GET">
                        <div class="has-feedback-left">
                            <input type="text" name="query" class="form-control" placeholder="{{ isset($query) ? $query : 'поиск' }}">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base"></i>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>
                    @if(!isset($query) || isset($type))
                        <a href="{{ route('admin.categories.sort', ['name' => 'id', 'type' => isset($type) ? $type : '']) }}">ID</a>
                    @else
                        ID
                    @endif
                </th>
                <th>
                    @if(!isset($query) || isset($type))
                        <a href="{{ route('admin.categories.sort', ['name' => 'name', 'type' => isset($type) ? $type : '']) }}">Name</a>
                    @else
                        Name
                    @endif
                </th>
                <th>Parent category</th>
                <th>Created_at</th>
                <th>Updated_at</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($result as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>

                    @if(isset($category->category->name))
                        <td>{{ $category->category->name }}</td>
                    @else
                        <td>Это родительская категория</td>
                    @endif

                    <td>{{ $category->created_at }}</td>
                    <td>{{ $category->updated_at }}</td>
                    <td class="text-center">
                        <div class="media-right media-middle" style="float: right">
                            <ul class="icons-list icons-list-extended text-nowrap">
                                <li><a href="{{ route('admin.categories.edit', ['id' => $category->id]) }}" class="edit"><button class="btn btn-primary"><i class="icon-pencil5"></i></button></a></li>
                                <li>
                                    <form action="{{ route('admin.categories.destroy', ['id' => $category->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('delete') }}
                                        <button type="submit" class="btn btn-danger delete"><i class="icon-trash-alt"></i></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>

        <div class="panel-footer">
            <div style="text-align: center">
                {{ $result->links() }}
            </div>
        </div>
    </div>
    <!-- /basic datatable -->

@endsection