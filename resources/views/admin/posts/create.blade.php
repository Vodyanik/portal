@extends('admin.layouts.app')

@section('script')
    <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_multiselect.js') }}"></script>
@endsection

@section('content')

    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{ route('admin.posts.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Создание поста</h5>
                    </div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <label>Миниатюра к посту</label>
                                <img id="outImage" src="{{ asset('images/default.jpg')}}" width="100%">
                                <input type="file" id="image" name="image" class="btn">
                                @if ($errors->has('image'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Заголовок</label>
                                    <input type="text" class="form-control" name="title" placeholder="Введите заголовок к посту" value="{{ old('title') }}">
                                    @if ($errors->has('title'))
                                        <span class="alert-danger">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Категория поста</label>
                                    <select name="category_id" class="bootstrap-select" data-live-search="true" data-width="100%">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <span class="alert-danger">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Содержимое поста</label>
                                    <textarea name="text" id="editor-text" rows="4" cols="4">{{ old('text') }}</textarea>
                                    @if ($errors->has('text'))
                                        <span class="alert-danger">
                                        <strong>{{ $errors->first('text') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Короткое описание поста</label>
                                    <textarea name="description" id="editor-description" style="width: 100%" >{{ old('description') }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="alert-danger">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Теги к посту</label>

                                    <div class="multi-select-full">
                                        <select class="multiselect-filtering" multiple="multiple" name="tag_ids[]">
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @if ($errors->has('category_id'))
                                        <span class="alert-danger">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Сохранить <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#image').change(function (e) {
                var file = e.target.files[0];
                if (!file.type.match('image.*')) {
                    alert("Image only please....");
                }
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#outImage').attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            });
        });

        CKEDITOR.replace( 'editor-text' );
        CKEDITOR.replace( 'editor-description' );
    </script>

@endsection