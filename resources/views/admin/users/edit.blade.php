@extends('admin.layouts.app')

@section('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_layouts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/uploader_bootstrap.js') }}"></script>
@endsection

@section('content')
    <!-- Vertical form options -->
    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <form action="{{ route('admin.users.update', ['id' => $user->id]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Редактирование пользователя </h5>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Image:</label>
                            <div class="row">
                                <div class="col-md-3">
                                    @if(!empty($user->image))
                                        <img id="outImage" src="{{ asset($user->image)}}" width="100%">
                                    @else
                                        <img id="outImage" src="{{ asset('images/default.jpg')}}" width="100%">
                                    @endif
                                </div>
                            </div>
                            <input type="file" id="image" name="image" class="btn">
                        </div>

                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" name="name" placeholder="name" value="{{ $user->name }}">
                        </div>

                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" class="form-control" name="email" placeholder="email" value="{{ $user->email }}">
                        </div>

                        <div class="form-group">
                            <label>Права пользователя:</label>
                            @foreach($roles as $role)
                                <div class="checkbox checkbox-switchery">
                                    <label>
                                        @if($role->users()->where('user_id', $user->id)->first() !== null)
                                            <input type="checkbox" name="{{ $role->name }}" class="switchery" checked>
                                        @else
                                            <input type="checkbox" name="{{ $role->name }}" class="switchery" unchecked>
                                        @endif
                                        {{ $role->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Сохранить <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#image').change(function (e) {
                var file = e.target.files[0];
                if (!file.type.match('image.*')) {
                    alert("Image only please....");
                }
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#outImage').attr('src', e.target.result);
                };
                reader.readAsDataURL(file);
            });
        });
    </script>

@endsection