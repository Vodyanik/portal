@extends('admin.layouts.app')

@push('script')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/ui/ripple.min.js') }}"></script>
@endpush

@section('content')

    <!-- Basic datatable -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                
                <div class="col-md-9">
                    <h5 class="panel-title"><a href="{{ route('admin.users.index') }}">Пользователи</a></h5>
                </div>
                
                <div class="col-md-3">
                    <form action="{{ route('admin.users.search') }}" method="GET">
                        <div class="has-feedback-left">
                            <input type="text" name="query" class="form-control" placeholder="{{ isset($query) ? $query : 'поиск' }}">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base"></i>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>
                        @if(!isset($query) || isset($type))
                            <a href="{{ route('admin.users.sort', ['name' => 'id', 'type' => isset($type) ? $type : '']) }}">ID</a>
                        @else
                            ID
                        @endif
                    </th>
                    <th>Image</th>
                    <th>
                        @if(!isset($query) || isset($type))
                            <a href="{{ route('admin.users.sort', ['name' => 'name', 'type' => isset($type) ? $type : '']) }}">Name</a>
                        @else
                            Name
                        @endif
                    </th>
                    <th>
                        @if(!isset($query) || isset($type))
                            <a href="{{ route('admin.users.sort', ['name' => 'email', 'type' => isset($type) ? $type : '']) }}">Email</a>
                        @else
                            Email
                        @endif
                    </th>
                    <th>Created_at</th>
                    <th>Updated_at</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach($result as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>
                            @if(!empty($user->image))
                                <img src="{{ asset($user->image) }}" width="100px">
                            @else
                                <img src="{{ asset('images/default.jpg') }}" width="100px">
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td>
                            @foreach($user->roles as $role)
                                @if($role->name == 'admin')
                                    <span class="label label-danger">{{ $role->name }}</span>
                                @elseif($role->name == 'moderator')
                                    <span class="label label-primary">{{ $role->name }}</span>
                                @endif
                            @endforeach
                        </td>
                        <td class="text-center">
                            <div class="media-right media-middle" style="float: right">
                                <ul class="icons-list icons-list-extended text-nowrap">
                                    <li><a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" class="edit"><button class="btn btn-primary"><i class="icon-pencil5"></i></button></a></li>
                                    <li>
                                        <form action="{{ route('admin.users.destroy', ['id' => $user->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                            <button type="submit" class="btn btn-danger delete"><i class="icon-trash-alt"></i></button>
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="panel-footer">
            <div style="text-align: center">
                {{ $result->links() }}
            </div>
        </div>
    </div>
    <!-- /basic datatable -->

@endsection